@if(session()->has('success'))
    <div class="confirmBox">
        @if(is_array(session()->get('success')))
            @foreach (session()->get('success') as $success)
                {{ $success }} <br>
            @endforeach
        @else
            {{ session()->get('success') }}
        @endif
    </div>
@endif

