<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body>
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="tableForm">
            <div class="dbimgcontainer" ></div>
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b>
                    <br/>Jesteś zalogowany jako <b>{{ $roleName }}</b>
                    <br/>Wybrana tabela: <b>{{ $tableName }}</b>
                    <br/>tryb modyfikowania
                </div> 
                <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables/crud/operation') }}">                
                    <input type="hidden" name="crudName" value="Modyfikuj">
                    <input type="hidden" name="modifyValuesCommand" value="whatever">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if ($tableName == 'Role')
                        @include('_rolesModify_')
                    @else
                        @include('_defaultModify_')
                    @endif
                    
                    <div class="buttonsContainer">
                        <button type="submit" class="positiveButton" >zaktualizuj</button>
                        <a href="./"><div class="returnButton">powrót</div></a>   
                    </div>
            </div>            
        </div>
    </body>
</html>





