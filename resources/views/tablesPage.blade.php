<?php 
    $translacja = [ 'Klienci' => 'Klienci',
                'Odbiory' => 'Odbiory',
                'Platnosci' => 'Płatności',
                'Samochody' => 'Samochody',
                'Sprzedawcy' => 'Sprzedawcy',
                'Zamowienia' => 'Zamówienia',
                'Role' => 'Role',
                'Uzytkownicy' => 'Użytkownicy',
                'RolaAUzytkownik' => 'Rola a użytkownik'
        
        
        
    ];
?>

<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body>
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="mainForm" >
            <div class="dbimgcontainer">                
            </div>
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b> <br/>Jesteś zalogowany jako <b>{{ $roleName }}</b>
                    </b><br/>Wybierz tabelę:</div> 
                
                @foreach ($tables as $tableName)
                 <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="tableName" value="{{ $tableName }}">
                    <button class="btn" type="submit">{{ $translacja[$tableName] }}</button></a> 
                </form>               
                @endforeach 
                
            </div>
        </div>
    </body>
</html>
