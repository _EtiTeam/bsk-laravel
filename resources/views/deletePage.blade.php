<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body>
        <!--<h2>Login Form</h2>-->
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="tableForm">
            <div class="dbimgcontainer" ></div>
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{Auth::user()->getAttribute('login')}}</b><br/> Jesteś zalogowany jako <b>{{ $roleName }}</b></br>Wybrana tabela: <b>{{ $tableName }}</b><br/>tryb usuwania</div>                 
                @if ($tableName == 'Role')
                  @include('_rolesDelete_')
                @else
                    
                <div class="tableContainer">
                <table>
                    <tr>
                        @foreach ($tableContent[0] as $element)                         
                        <th>{{ $element[0] }}</th>
                        @endforeach 	
                    </tr>
                    @foreach ($tableContent as $row)  
                    
                    <tr>
                        @foreach ($row as $element)
                            @if ($element[5]) <!--printing id and description if exists-->
                                <td>{{ "$element[1] ($element[5])" }}</td>
                            @else
                                <td>{{ $element[1] }}</td>
                            @endif
                        @endforeach 
                    <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables/crud/operation') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="crudName" value="Usuń">
                        <input type="hidden" name="deleteSelectedRow" value="Usuń">
                        <td class="deleteCell">
                            <input type="hidden" name="deleteRowID" value="{{$row[0][1]}}">
                            <input type="hidden" name="deleteColumnName" value="{{$row[0][0]}}">
                            @if (array_key_exists(1, $row))
                                <input type="hidden" name="deleteRowID2" value="{{$row[1][1]}}">
                                <input type="hidden" name="deleteColumnName2" value="{{$row[1][0]}}">
                            @endif
                            <button class="hiddenButton"  type="submit">
                                <img class="deleteIcon" src="{{ asset('img/del.png')}}"/>
                            </button>
                        </td>
                    </form>
                    </tr>
                    
                    @endforeach   
                </table>
                </div>
                    
                @endif
                <div class="buttonsContainer">
                <a href="./"><div class="returnButton">powrót</div></a>
                </div>
            </div>            
        </div>
    </body>
</html>





