@if(isset($newPassword))
<div class="infobox">
    Stworzono nowego użytkownika!</br> 
    <b>Login:</b> {{$newLogin}} </br>
    <b>Hasło:</b> {{$newPassword}}</br>
    Po pierwszym zalogowaniu stworzony użytkownik będzie musiał zmienić hasło
</div>
@endif
