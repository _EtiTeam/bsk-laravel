<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>

    <body>
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="tableForm">
            <div class="dbimgcontainer" >
            </div>	
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b><br/> Jesteś zalogowany jako <b>{{ $roleName }}</b></br>Wybrana tabela: <b>{{ $tableName }}</b><br/>tryb dodawania</div> 
                @include('userCreateInfo')
                
                 @if ($tableName == 'Role')
                        @include('_rolesCreate_')
                    @else
                        @include('_defaultCreate_')
                    @endif
                
                <div class="buttonsContainer">
                <button type="submit" class="positiveButton" >dodaj wpis</button>
                </form>  
                <a href="./"><div class="returnButton">powrót</div></a>
                </div>
            </div>
        </div>
    </body>
</html>
