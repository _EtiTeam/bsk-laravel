<!DOCTYPE html>
<?php 
    $translacja = [ 'Read' => 'Odczyt',
                'Create' => 'Dodawanie',
                'Update' => 'Aktualizacja',
                'Delete' => 'Usuwanie'
        
    ];
?>
<html>
    <head>
        @include('header')
    </head>
    <body>
        @include('static/errors')
        @include('static/loggedDashboard')
        <div class="mainForm">
            <div class="dbimgcontainer">                
            </div>
            <div class="container">
                
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b> <br/> Jesteś zalogowany jako <b>{{ $roleName }}</b>  <br/>Wybrana tabela: <b>{{ $tableName }}</b><br/>Wybierz operację:</div>
                @foreach ($cruds as $crudName)
                   <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables/crud') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="crudName" value="{{ $crudName }}">
                    <button class="btn" type="submit">{{ $translacja[$crudName] }}</button></a> 
                </form>      
                
                @endforeach 	 
                <a href="./"><div class="returnButton">powrót</div></a>    
            </div>            
        </div>

    </body>
</html>
