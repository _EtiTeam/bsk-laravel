<div class="tableContainer">
                <table>
                    <tr>
                        <!--headlines-->
                        @foreach ($tableContent[0] as $element)                         
                        <th>{{ $element[0] }}</th>
                        @endforeach 	
                    </tr>
                    <!--elements-->
                     @foreach ($tableContent as $row)   
                    <tr>
                        @foreach ($row as $element)
                            @if ($element[5]) <!--printing id and description if exists-->
                                <td>{{ "$element[1] ($element[5])" }}</td>
                            @else
                                <td>{{ $element[1] }}</td>
                            @endif
                        @endforeach 
                    </tr>
                    @endforeach     
                </table>
                </div>