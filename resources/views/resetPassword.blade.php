<!DOCTYPE html>
<html>
<head>
@include('header')
</head>


<body>
@include('static/errors')
@include('static/success')

<!--<h2>Login Form</h2>-->
<a href="/logout"><div class="logout">WYLOGUJ</div></a>
<form class="mainForm" action="{{{url('/reset')}}}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="imgcontainer">
    <!--<img src="img_avatar2.png" alt="Avatar" class="avatar">-->
	<!--<img src="img/lock.ico" alt="lockIcon" class="avatar">-->
  </div>
 
  <div id="messageBox" style="display: none; border-color:red;font-size:16pt;" class="infobox">Błąd</div> 
  <div class="container">
      <div class ="loginContainer">
        <label><b>stare hasło</b></label>
        <input id="login" type="password" placeholder="stare hasło" name="oldPassword" required>

        <label><b>nowe hasło</b></label>
        <input id="password" type="password" placeholder="nowe hasło" name="password" minlength="8" required>
        <label><b>powtórz nowe hasło</b></label>
        <input id="password" type="password" placeholder="powtórz nowe hasło" name="password_confirmation" minlength="8" required>
      </div>
      <div class="buttonsContainer">
           <button type="submit" id="resetPasswordSubmit">zmień hasło</button>
                <a href="./"><div class="returnButton">powrót</div></a>  
        </div>
   
    <!---<input type="checkbox" checked="checked"> Remember me -->
  </div>
<!--
  <div class="container" style="background-color:#f1f1f1">
    <button type="button" class="cancelbtn">Cancel</button>
    <span class="psw">Forgot <a href="#">password?</a></span>
  </div>
 --> 
</form>
<!--<script>
$( "#login" ).click(function() {
	//alert("fdsf");
  $( "#messageBox" ).hide( "fast", function() {
    // Animation complete.
  });
});
$( "#password" ).click(function() {
	//alert("fdsf");
  $( "#messageBox" ).show( "fast", function() {
    // Animation complete.
  });
});
</script>-->
</body>
</html>
