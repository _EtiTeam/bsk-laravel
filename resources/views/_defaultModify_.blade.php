<div class="tableContainer">
    <table>
        <tr>
            @foreach ($tableContent[0] as $element)                         
            <th>{{ $element[0] }}</th>
            @endforeach 	
        </tr>
        @for($j = 0; $j < count($tableContent); $j++)   
        <tr>
            @for ($i = 0; $i < count($tableContent[$j]); $i++)
            <!--DATE-->
            @if ($tableContent[$j][$i][2] === 'date')
            <!--<td> <div style="text-align: center;">{{ $tableContent[$j][$i][1] }}</div></td>-->
            <!--<td><input type ="" name="{{ $j.'#'.$i}}" placeholder="{{ $tableContent[$j][$i][1] }}" value="{{ $tableContent[$j][$i][1] }}"></input></td>-->
                <!--<td><input type="text" name="{{ $j.'#'.$i}}"  placeholder=" rrrr-mm-dd hh:mm:ss.xxx " value="{{ $tableContent[$j][$i][1] }}"></input></td>-->    
            <td><input type="text" name="{{ $j.'#'.$i}}"  placeholder=" rrrr-mm-dd" value="{{ $tableContent[$j][$i][1] }}"></input></td>    
            <!--CHECKBOX-->
            @elseif ($tableContent[$j][$i][2] === 'checkbox' && $tableContent[$j][$i][1] === '1')
            <td><input type="{{$tableContent[$j][$i][2]}}"name="{{ $j.'#'.$i}}" checked></input></td>
            <!--isIdentity-->
            @elseif ($tableContent[$j][$i][4]) 
            <td><input type="{{$tableContent[$j][$i][2]}}"name="{{ $j.'#'.$i}}" placeholder="{{ $tableContent[$j][$i][1] }}" value="{{ $tableContent[$j][$i][1] }}" disabled></input></td>
            @else      
                <?php 
                    $element = $tableContent[$j][$i]; 
                ?>
                <td>
                
                @if ($element[6]) <!--printing id and description if exists-->             
                    <select name="{{ $j.'#'.$i}}">
                        @foreach($element[6] as $id => $description)
                            @if ($id.'' === $element[1]) <!--selected option-->
                                <option value='{{ $id }}' selected="selected">{{ "$id ($description)" }}</option>
                            @else
                                <option value='{{ $id }}'>{{ "$id ($description)" }}</option>
                            @endif
                        @endforeach

                    </select>
                @else
                    <input type="{{ $element[2] }}" name="{{ $j.'#'.$i}}" placeholder="{{ $element[1] }}" value="{{ $element[1] }}"></input>
                @endif
                
                </td>
            @endif
            @endfor
        </tr>
        @endfor 
    </table>
</div>