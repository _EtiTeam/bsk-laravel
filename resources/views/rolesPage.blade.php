<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body>    
        @include('static/errors')
        @include('static/loggedDashboard')
        @include('static/success')
        <div class="mainForm">
            <div class="dbimgcontainer"></div>
            <div class="container">
                <div class="infobox">Witaj użytkowniku <b>{{ Auth::user()->getAttribute('login') }}</b> <br/>Wybierz rolę:</div> 

                @foreach ($table as $user)
                <form class="hiddenForm" role="form" method="POST" action="{{ url('/roles') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="roleName" value="{{ ($nazwaRoli=$user['NazwaRoli']) }}">
                    <button class="btn" type="submit">{{ $nazwaRoli=$user['NazwaRoli'] }}</button></a> 
                </form>
                @endforeach 	
                
            </div>
        </div>

    </body>
</html>
