<div class="tableContainer">
                <table>
                    <tr>
                        @foreach ($tableColumns as $element)                         
                        <th>{{ $element[0] }}</th>
                        @endforeach 	
                    </tr>
                      <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables/crud/operation') }}">
                    <tr>
                        
                         @foreach ($tableColumns as $element) 
                       
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="crudName" value="Dodaj">
                            <input type="hidden" name="addValuesCommand" value="whatever">
                            @if ($element[1] === 'date')
                                <!--<td><input type="text" name="{{ $element[0] }}" placeholder="rrrr-mm-dd hh:mm:ss.xxx" value="{{date('Y-m-d H:i:s')}}" style="width: 100%"></input></td>-->
                                <td><input type="text" name="{{ $element[0] }}" placeholder="rrrr-mm-dd" value="{{date('Y-m-d')}}" style="width: 100%"></input></td>
                            @else
                                @if ($element[2])
                                    <td><input type="{{$element[1]}}" name="{{ $element[0] }}" placeholder="{{ $element[0] }}" style="width: 100%" disabled></input></td>
                                @elseif ($element[0]==="CzyZalogowany")
                                    <td><input type="{{$element[1]}}" name="{{ $element[0] }}" placeholder="{{ $element[0] }}" style="width: 100%" value="0" disabled></input></td>
                                @else
                                    <td>
                                    @if ($element[3]) <!--printing id and description if exists-->             
                                        <select name="{{ $element[0] }}">
                                            
                                            <option value=''></option> <!-- empty option-->
                                            @foreach($element[3] as $id => $description)
                                                <option value='{{ $id }}'>{{ "$id ($description)" }}</option>
                                            @endforeach

                                        </select>
                                    @else
                                        <input type="{{$element[1]}}" name="{{ $element[0] }}" placeholder="{{ $element[0] }}" style="width: 100%"></input>
                                    @endif
                                    </td>
                                @endif
                            @endif
                         @endforeach  
                            
                    </tr>
                </table>
                </div>