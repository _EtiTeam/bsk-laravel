<div class="tableContainer">
                <table>
                    <tr>
                        
                        @for ($nr_kolumny = 0; $nr_kolumny < 2; $nr_kolumny++)                        
                        <th>{{ $tableContent[0][$nr_kolumny][0] }}</th>
                        @endfor
                    </tr>
                    @foreach ($tableContent as $row)  
                    
                    <tr>
                        @for ($nr_kolumny = 0; $nr_kolumny < 2; $nr_kolumny++)
                            @if ($row[$nr_kolumny][5]) <!--printing id and description if exists-->
                                <td>{{ "$element[1] ($element[5])" }}</td>
                            @else
                                <td>{{ $row[$nr_kolumny][1] }}</td>
                            @endif
                        @endfor
                    <form class="hiddenForm" role="form" method="POST" action="{{ url('/tables/crud/operation') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="crudName" value="Usuń">
                        <input type="hidden" name="deleteSelectedRow" value="Usuń">
                        <td class="deleteCell">
                            <input type="hidden" name="deleteRowID" value="{{$row[0][1]}}">
                            <input type="hidden" name="deleteColumnName" value="{{$row[0][0]}}">
                            <button class="hiddenButton"  type="submit">
                                <img class="deleteIcon" src="{{ asset('img/del.png')}}"/>
                            </button>
                        </td>
                    </form>
                    </tr>
                    
                    @endforeach   
                </table>
                </div>