<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('login', 50)->unique();
                        
                        $table->string('password', 128);
                        $table->string('salt')->nullable(); // TODO do zmiany
                        
                        $table->boolean('CzyZalogowany')->default(false);
                        $table->dateTime("DateTimeOstAktywnosci")->nullable();
                        
                        $table->integer("ZalogowanyNaRole")->nullable();

			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
