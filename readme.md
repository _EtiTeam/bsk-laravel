# Projekt Bezpieczeństwa Systemów Komputerowych #
### Podstawowa specyfikacja ###
* realizacja modelu Role-Based Access Control
* wykorzystanie serwera Microsoft SQL Server
* wykorzystanie PHP z framework Laraverl
* dostęp do systemu za pomocą strony internetowej
* architektura MVC

### Kwestie bezpieczeństwa ###
* odporny na SQL Injection
* weryfikacja uprawnień po stronie serwera PHP przy każdej czynności
* ukrycie historii przeglądania (tylko żądania POST)
* ochrona przed XSS, CSRF
* wyeliminowanie JavaScript uznawanego za niebezpieczny
* animacje komunikatów zaimplementowane w CSS 3.0
* wymuszona obsługa HTTPS
* stosowanie sha512
* stosowanie soli do przechowywania haseł
* stosowanie zabezpieczeń po stronie bazy danych (triggery, widoki)
* wymuszenie długich haseł
* konieczność zmiany hasła przy pierwszym logowaniu