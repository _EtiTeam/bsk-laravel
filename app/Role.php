<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    //public static function getRoleFromName($roleName){
    //    return Role::firstByAttributes(['NazwaRoli' => "$roleName"]);
    //} 
    
    const ID_COLUMN = 'IDRola';
    const TABLE_NAME = 'Role';
    protected $table = Role::TABLE_NAME;
    
    /**
     * The database table primary key column name
     * 
     * @var string 
     */
    protected $primaryKey = Role::ID_COLUMN;

    protected $guarded = ['IDRola'];
    
    public $timestamps = false;

}
