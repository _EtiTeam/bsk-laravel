<?php

namespace App;

use DB;
use Auth;

use App\User;

/**
 * Representation of tables form database
 *
 * @author Daniel
 */
abstract class DbTables {
    
    /**
     * 
     * @param User $user
     * @return array
     */
    public static function getTablesForUser(User $user){
        $array = array();
        foreach(DbTables::getTableNames() as $tableName){
            $a = DbTables::hasUserAccessToTable($user, $tableName);
            if(DbTables::hasUserAccessToTable($user, $tableName))
                array_push ($array, $tableName);
        }
        return $array;
    }
    
    public static function refreshDb(){
        foreach (User::all() as $user)
            $user->refresh();
    }
    
    public static function hasUserAccessToTable(User $user, $tableName){
        return $user->hasAccessTo($tableName);
    }
    
    public static function contains($tableName){
        $tableNames = DbTables::getTableNames();
        return in_array($tableName, $tableNames);
    }
    
    public static function getTableViewNames(){
        return array_values(DbTables::$TABLE_NAMES);
    }
    
    public static function getTableNames(){
        return array_keys(DbTables::$TABLE_NAMES);
    }
    
    public static function getViewName($tableName){
        return DbTables::$TABLE_NAMES[$tableName];
    }
    
    public static function getColumnNames($tableName){
        //$sqlQuery = "SELECT COLUMN_NAME"
        //        . "FROM bskDB.INFORMATION_SCHEMA.COLUMNS"
        //        . "WHERE TABLE_NAME = N'".UzytkownicyView."'";
        return DB::table('INFORMATION_SCHEMA.COLUMNS')
                ->where('TABLE_NAME', '=', "$tableName")
                ->lists('COLUMN_NAME');
    }
    

    private static function getDescription($tableName, $columnName){
        if (!array_key_exists($tableName, DbTables::$DECRIPTION_COLUMNS)) 
            return false;
        $search1 = DbTables::$DECRIPTION_COLUMNS[$tableName];

        if (!array_key_exists($columnName, $search1)) 
            return false;
        $search2 = $search1[$columnName];
        return $search2;
    }
    
    private static function getDescriptionTableName($tableName, $columnName){
        $description = DbTables::getDescription($tableName, $columnName);
        if (!$description) return false;
        return $description['table'];
    }
    
    private static function getDescriptionSourceColumnName($tableName, $columnName){
        $description = DbTables::getDescription($tableName, $columnName);
        if (!$description) return false;
        return $description['sourceColumn'];
    }
    
    private static function getDescriptionColumnName($tableName, $columnName){
        $description = DbTables::getDescription($tableName, $columnName);
        if (!$description) return false;
        return $description['descriptionColumn'];
    }
    
    
    public static function getDecriptionValue($tableName, $columnName, $columnValue){
        $descriptionTable = DbTables::getDescriptionTableName($tableName, $columnName);
        $descriptionSourceColumn = DbTables::getDescriptionSourceColumnName($tableName, $columnName);
        $descriptionColumn = DbTables::getDescriptionColumnName($tableName, $columnName);

        if (!$descriptionSourceColumn || !$descriptionTable || !$descriptionColumn)
            return false;
        
        $readPermission = Auth::user()->getPermissions($descriptionTable)->isReadPermission();
        if (!$readPermission)
            return false;

        $value = DB::table($descriptionTable)
                ->where($descriptionSourceColumn, $columnValue)
                ->pluck($descriptionColumn);
        
        if (empty($value)) 
            return false; // if reference is null
        return $value;
    }
    
    public static function getDescriptionPossibleValues($tableName, $columnName){
        $descriptionTable = DbTables::getDescriptionTableName($tableName, $columnName);
        $descriptionSourceColumn = DbTables::getDescriptionSourceColumnName($tableName, $columnName);
        $descriptionColumn = DbTables::getDescriptionColumnName($tableName, $columnName);

        if (!$descriptionSourceColumn || !$descriptionTable || !$descriptionColumn)
            return false;

        $readPermission = Auth::user()->getPermissions($descriptionTable)->isReadPermission();
        if (!$readPermission)
            return false;
        
        $value = DB::table($descriptionTable)
            //->where($descriptionSourceColumn, $columnValue)
            ->lists($descriptionColumn, $descriptionSourceColumn);
        
        if (empty($value)) 
            return false; // if reference is null
        return $value;
    }
    
    
    private static $TABLE_NAMES = [
        //$tableName => viewName (if it not exist use $tableName)
        'Klienci' => 'Klienci',
        'Odbiory' => 'Odbiory',
        'Platnosci'=> 'Platnosci',
        'Samochody' => 'Samochody',
        'Sprzedawcy' => 'Sprzedawcy',
        'Zamowienia' => 'Zamowienia',
        
        Role::TABLE_NAME => Role::TABLE_NAME,
        User::TABLE_NAME => User::TABLE_VIEW_NAME,
        'RolaAUzytkownik' => 'RolaAUzytkownik',
    ];
    

    
    /**
     *  [TABLE_NAME] => [
     *      [COLUMN_NAME] => [
     *          // data
     *          ['table']
     *          ['sourceColumn']
     *          ['description']
     *      ]
     *  ]
     * 
     * @var array
     */
    private static $DECRIPTION_COLUMNS = [
        'RolaAUzytkownik' =>[
            'IDRola' => [
                'table' => Role::TABLE_NAME,
                'sourceColumn' => Role::ID_COLUMN,
                'descriptionColumn' => 'NazwaRoli', // value that is showing with id key
            ],
            'IDUzytkownik' => [
                'table' => User::TABLE_NAME,
                'sourceColumn' => User::ID_COLUMN,
                'descriptionColumn' => 'login', // value that is showing with id key
            ],
            
        ],
        'Zamowienia' =>[
            'PESEL_s' => [
                'table' => 'Sprzedawcy',
                'sourceColumn' => 'PESEL',
                'descriptionColumn' => 'Nazwisko', // value that is showing with id key
            ],
            'PESEL_k' => [
                'table' => 'Klienci',
                'sourceColumn' => 'PESEL',
                'descriptionColumn' => 'Nazwisko', // value that is showing with id key
				
            ],
            'IDSamochodu' => [
                'table' => 'Samochody',
                'sourceColumn' => 'IDSamochodu',
                'descriptionColumn' => 'Model', // value that is showing with id key
            ], 
            'IDOdbioru' => [
                'table' => 'Odbiory',
                'sourceColumn' => 'IDOdbioru',
                'descriptionColumn' => 'Data', // value that is showing with id key
            ], 
            
        ],
    ];
}
