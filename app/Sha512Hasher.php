<?php

namespace App;

use Illuminate\Contracts\Hashing\Hasher as HasherContract;
/**
 * Description of Sha512Hasher
 *
 * @author Daniel
 */

class Sha512Hasher implements HasherContract {

      public function make($value, array $options = array()) {
          $value = env('SALT', '').$value;
          return hash('sha512', $value);
      }

      public function check($value, $hashedValue, array $options = array()) {
          return $this->make($value) === $hashedValue;
      }

      public function needsRehash($hashedValue, array $options = array()) {
          return false;
      }

  }