<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);



//Route::get('/db', 'TryDBController@index');
//Route::get('/', 'LoginPageController@index');
//Route::post('/roles', 'DatabaseController@rolesPage');

//Route::post('/tables', 'DatabaseController@tablesPage');
//Route::get('/tables', 'DatabaseController@tablesPage');

//Route::post('/tables/operations', 'DatabaseController@crudPage');
//Route::get('/tables/operations', 'DatabaseController@crudPage');


//Route::get('/{tableName}/{crudName}', 'DatabaseController@crudFunctions');
//Route::get('/user/{roleName}', [
//    'as' => $roleName, 'uses' => 'DatabaseController@tablesPage'
//]);

//Route::get('/db', ['middleware' => 'auth', 'uses' => 'TryDBController@index']);

Route::get('/dbError', ['middleware' => 'App\Http\Middleware\DB\RedirectIfDbOk', function(){
    return view('errors/dbError');
}]);

Route::group(['middleware' => '\App\Http\Middleware\DB\CheckDbConnection'], function()
{
    /* User Authentication */
    Route::get('/', 'Auth\AuthController@getLogin');
    Route::post('/', 'Auth\AuthController@postLogin');
    Route::get('/logout', 'Auth\AuthController@getLogout');
    
    //Route::get('register', 'Auth\AuthController@getRegister');
    //Route::post('register', 'Auth\AuthController@postRegister'); 

    /* Authenticated users */
    Route::group(['middleware' => 'auth'], function()
    {
        Route::get('/reset', 'Auth\PasswordController@getResetPassword');        
        Route::post('/reset', 'Auth\PasswordController@postResetPassword');
        
        Route::group(['middleware' => '\App\Http\Middleware\PasswordChangeRequired'], function()
        {
            Route::get('/roles', 'RoleController@index');    
            Route::post('/roles', 'RoleController@store');

            Route::group(['middleware' => '\App\Http\Middleware\RoleSelected'], function()
            {
                Route::get('/tables', 'TableController@index');
                Route::post('/tables', 'TableController@store');

                Route::group(['middleware' => '\App\Http\Middleware\TableSelected'], function(){
                    Route::get('/tables/crud', 'CrudController@index');
                    Route::post('/tables/crud', 'CrudController@store');

                    Route::group(['middleware' => '\App\Http\Middleware\CrudSelected'], function(){
                        Route::post('/tables/crud/operation', 'DatabaseController@crudChoose');
                        Route::get('/tables/crud/operation', 'DatabaseController@crudChoose');
                    });
                });
            });
        });
    });
});
