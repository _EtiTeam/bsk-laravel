<?php namespace App\Http\Middleware\DB;

use App\Http\Controllers\TableController;

use Closure;
use Illuminate\Http\RedirectResponse;
use \Illuminate\Contracts\Routing\Middleware;


class RedirectIfDbOk implements Middleware {

    const SUCCESS_REDIRECTION = '/';
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (CheckDbConnection::isDatabaseOk())
            return new RedirectResponse(url($this::SUCCESS_REDIRECTION));

        return $next($request);
    }

}
