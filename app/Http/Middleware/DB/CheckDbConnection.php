<?php namespace App\Http\Middleware\DB;

use DB;
use Schema;
use Closure;
use \Illuminate\Contracts\Routing\Middleware;
use Illuminate\Http\RedirectResponse;
use App\DbTables;


class CheckDbConnection implements Middleware {

    const FAILURE_REDIRECTION = '/dbError';
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->isDatabaseOk())
        {
            return new RedirectResponse(url($this::FAILURE_REDIRECTION));
        }

        return $next($request);
    }
    
    public static function isDatabaseOk(){
        try{
            // check db connection
            if(!DB::connection()->getDatabaseName())
                return false;

            // check if db schema is correct
            foreach (DbTables::getTableNames() as $tableName){
                if(!Schema::hasTable($tableName))
                    return false;
            }
            return true;
            
        } catch(\Exception $e) {
            return false;
        }
    }

}
