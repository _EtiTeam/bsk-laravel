<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use \Illuminate\Contracts\Routing\Middleware;

use App\Http\Controllers\Auth\PasswordController;


class PasswordChangeRequired implements Middleware {

    
    const SUCCESS_REDIRECTION = PasswordController::FAILURE_REDIRECTION;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->getIsRequiredPasswordChange()){
            return redirect()->guest($this::SUCCESS_REDIRECTION)
                ->withErrors('Przed kontynuowaniem pracy musisz zmienić swoje hasło');
        }

        return $next($request);
    }
}
