<?php namespace App\Http\Middleware;

use App\Http\Controllers\TableController;

use Closure;
use Illuminate\Http\RedirectResponse;
use \Illuminate\Contracts\Routing\Middleware;


class RedirectIfTableSelected implements Middleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (TableController::isTableSelected())
            return new RedirectResponse(url(TableController::SUCCESS_REDIRECTION));

        return $next($request);
    }

}
