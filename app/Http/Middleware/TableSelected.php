<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use \Illuminate\Contracts\Routing\Middleware;

use App\SessionFields;
use App\Http\Controllers\TableController;

class TableSelected implements Middleware {

    //public function __construct(Guard $auth) {
    //}
    
    const FAILURE_REDIRECT = TableController::FAILURE_REDIRECTION;
    //const SUCCESS_REDIRECT = TableController::SUCCESS_REDIRECTION;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::regenerate();
        $sessionTableName = Session::get(SessionFields::TABLE_NAME);
        
        // if terminal has't got any table (in session)
        if (is_null($sessionTableName)) 
           return redirect()->guest($this::FAILURE_REDIRECT);

        // check if user has access to this table (it also check if it exists)
        if (!Auth::user()->hasAccessTo($sessionTableName)){
            Session::remove(SessionFields::TABLE_NAME);
            return redirect()->guest($this::FAILURE_REDIRECT);
        }

        return $next($request);
    }

}
