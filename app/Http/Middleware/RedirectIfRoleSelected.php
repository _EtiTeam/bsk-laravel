<?php namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\RoleController;
use \Illuminate\Contracts\Routing\Middleware;
use Illuminate\Http\RedirectResponse;



class RedirectIfRoleSelected implements Middleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (RoleController::isRoleSelected())
        {
            return new RedirectResponse(url(RoleController::SUCCESS_REDIRECTION));
        }

        return $next($request);
    }

}
