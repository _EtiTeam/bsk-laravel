<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use \Illuminate\Contracts\Routing\Middleware;

use App\Permissions;
use App\SessionFields;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\TableController;

class CrudSelected implements Middleware {

    const FAILURE_REDIRECT = CrudController::FAILURE_REDIRECTION;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::regenerate();
        $crudOperationName = Session::get(SessionFields::CRUD_NAME);
        
        // if terminal hasn't selected any crud operation (in session)
        if (is_null($crudOperationName)) 
            return redirect()->guest($this::FAILURE_REDIRECT)
                ->withErrors("Błąd wyboru operacji");

        // check if string from input is a right crud operation name
        if (!Permissions::isCrudOperation($crudOperationName)){
            Session::remove(SessionFields::CRUD_NAME);
            return redirect($this::FAILURE_REDIRECTION)->withErrors("Błąd wyboru operacji");
        }
            
        // check if user has permissions to this operation
        $tableName = TableController::getSelectedTableName();
        $isUserHasThisPermission = Auth::user()
                ->getPermissions($tableName)
                ->isPermission($crudOperationName);
        if (!$isUserHasThisPermission){
            Session::remove(SessionFields::CRUD_NAME);
            return redirect($this::FAILURE_REDIRECTION)
                ->withErrors("Błąd wyboru operacji");
        }
            
        return $next($request);
    }

}
