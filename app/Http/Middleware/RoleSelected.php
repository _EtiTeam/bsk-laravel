<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;
use \Illuminate\Contracts\Routing\Middleware;

use App\SessionFields;
use App\Http\Controllers\RoleController;

class RoleSelected implements Middleware {

    //public function __construct(Guard $auth) {
    //}
    
    const FAILURE_REDIRECT = RoleController::FAILURE_REDIRECTION;
    //const SUCCESS_REDIRECT = RoleController::SUCCESS_REDIRECTION;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Session::regenerate();
        $sessinRoleID = RoleController::getSelectedRoleId();
        
        // if terminal has't got any role (in session)
        if (is_null($sessinRoleID)) 
           return redirect()->guest($this::FAILURE_REDIRECT);

       // if user isn't logged as any role (in database)
       if (is_null(Auth::user()->role)){
           Session::remove(SessionFields::ROLE_ID);
           return redirect()->guest($this::FAILURE_REDIRECT);
       }

       // fetch dictionary 'NazwaRoli' => 'IDRola' for this user (based on his roles)
       //$userRoles = Auth::user()->roles->lists('IDRola', 'NazwaRoli');

       // check if user has this role
       //$role = Auth::user()->roles->where('NazwaRoli', $sessuibRoleID)->first();
       $role = Auth::user()->roles->find($sessinRoleID);
       if (is_null($role)){
           Session::remove(SessionFields::ROLE_ID);
           return redirect()->guest($this::FAILURE_REDIRECT);
       }

       // check if user is logged as this role
       if (Auth::user()->role->getKey() !== $role->getKey()){
           Session::remove(SessionFields::ROLE_ID);
           return redirect()->guest($this::FAILURE_REDIRECT);
//                   ->withErrors("Jesteś już zalogowany na inną rolę");
       }

        return $next($request);
    }

}
