<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\User;
use Auth;
use App\Http\Controllers\Auth\AuthController;
use Config;

class Authenticate {

    const FAILURE_REDIRECT = AuthController::FAILURE_REDIRECTION;
    
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
            $this->auth = $auth;
    }

    /**
     * Checking if user must be logout because of timeout (in database)
     * 
     * @param User $user
     * @return boolean
     */
    public static function isTooLongInactive(User $user){
        $sessionLifetime = Config::get('session.lifetime');
        $lastActivityDateTime = $user->getLastActivityTime();
        $elapsedTime =  (new \DateTime("now"))->getTimestamp() - $lastActivityDateTime->getTimestamp(); 
        $elapsedMinuts = $elapsedTime / 60.0; // it was in seconds
        return $elapsedMinuts > $sessionLifetime;
    } 

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->auth->check())
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->guest($this::FAILURE_REDIRECT);
            }
        }

        /**
         * @var App\User
         */
        $user = Auth::user();

        // checking if anyone is logged as this user (in database)
        $isLogged = $user->getIsAnyoneLoggedIn();
        if($isLogged === false){
            AuthController::logout();
            return redirect()->guest($this::FAILURE_REDIRECT)
                    ->withErrors("Zostałeś wylogowany");
        }

        // logout all user because of too long inactive (in database)
        if($this->isTooLongInactive($user)){
            AuthController::logoutAllTerminals();
            return redirect()->guest($this::FAILURE_REDIRECT)
                    ->withErrors("Zostałeś wylogowany");
        }

        // update last activity time
        $user->updateActivityTime();

        return $next($request);
    }

}
