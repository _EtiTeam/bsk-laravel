<?php namespace App\Http\Controllers;


use Auth;
use Input;
use Session;

use App\Translator;
use App\Permissions;
use App\SessionFields;
use App\Http\Controllers\Controller;

class CrudController extends Controller {

    const FAILURE_REDIRECTION = '/tables/crud';
    const SUCCESS_REDIRECTION = '/tables/crud/operation';
    
    public function __construct() {
        //$this->middleware('App\Http\Middleware\RedirectIfCrudSelected');
    }
    
    public function index()
    {
        $roleName = RoleController::getSelectedRoleName();
        $tableName = TableController::getSelectedTableName();
       // $tableName = Translator::tablesToFrontend($tableName);

        $crudPermissions = Auth::user()
                ->getPermissions($tableName)
                ->getPermissionNames();
        //$crudPermissions = Translator::crudsToFrontend($crudPermissions);

        return view('crudPage', [   'cruds' => $crudPermissions,
                                    'tableName' => $tableName,
                                    'roleName' => $roleName ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {   
        if (!Input::has('crudName'))
            return redirect($this::FAILURE_REDIRECTION)->withErrors("Błąd wyboru operacji");
        $crudOperation = Input::get('crudName');
        
        // check if string from input is a right crud operation name
        if (!Permissions::isCrudOperation($crudOperation))
            return redirect($this::FAILURE_REDIRECTION)->withErrors("Błąd wyboru operacji");
        
        // check if user has permissions to this operation
        $tableName = TableController::getSelectedTableName();
        $isUserHasThisPermission = Auth::user()
            ->getPermissions($tableName)
            ->isPermission($crudOperation);
        if (!$isUserHasThisPermission)
            return redirect($this::FAILURE_REDIRECTION)->withErrors("Błąd wyboru operacji");
        
        //saving to session -> only for check this in middleware
        Session::regenerate();
        Session::put(SessionFields::CRUD_NAME, $crudOperation);
        return redirect()->guest($this::SUCCESS_REDIRECTION);
    }
    
    /**
     * 
     * @return string|null
     */
    public static function getSelectedCrudOperation(){
        Session::regenerate();
        return Session::get(SessionFields::CRUD_NAME);
    }

    /**
     * 
     * @return boolean
     */
    public static function isCrudOperationSelected(){
        Session::regenerate();
        return Session::has(SessionFields::CRUD_NAME);
    }

}
