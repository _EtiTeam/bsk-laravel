<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Cache;
use Input;
use Session;
use Illuminate\Database\QueryException;

use App\Role;
use App\User;
use App\DbTables;
use App\Translator;
use App\Permissions;
use App\SessionFields;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\NotificationsController;

class DatabaseController extends Controller {

        const FAILURE_REDIRECTION = '/tables/crud';
        const THIS_REDIRECTION = '/tables/crud/operation';
        private $primaryKeysInRow = 0;
        //avaible options
        //public $cruds = ["Przeglądaj","Dodaj","Usuń","Modyfikuj"];
	public function __construct()
	{
	}
        
               
        public static function getFieldType($table, $columnName) 
        {
            
            $sql = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$columnName."'";
           
            try {
                $pdo = DB::connection()->getPdo();
                $result = $pdo->query($sql);
                foreach ($pdo->query($sql) as $row) 
                {
                    return $row[0];
                    
                }
                ////var_dump($resulty);
            } catch (PDOException $e) {
                self::logError($e, "getFieldType", $sql);
            } 
        }  
		
        public static function isPrimaryKey($tableName, $columnName) 
        {
            $sql = "SELECT COLUMN_NAME FROM bskDB.INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME LIKE '".$tableName."' AND CONSTRAINT_NAME LIKE 'PK%'";
           
            try {
                $pdo = DB::connection()->getPdo();
                $result = $pdo->query($sql);
                $primaryKey;
                foreach ($pdo->query($sql) as $row) 
                {
                     $primaryKey = $row[0];
                    
                }
                if ($columnName === $primaryKey)
                {
                    return true;
                }
                
                return false;
                ////var_dump($resulty);
            } catch (PDOException $e) {
                self::logError($e, "isPrimaryKey", $sql);
            }
        }   
       
		
        public static function mapTypeToHTML($type)
        {   
            if($type === 'datetime') return 'date-local';
            if($type === 'date') return 'date';            
            if($type === 'bit') return 'checkbox';
            if($type === 'int') return 'number';
            else return 'text';
        }


        //////////////////////////
        //// Layout section   ////
        //////////////////////////    


        
        //////////////////////////
        ////   Read section   ////
        //////////////////////////       
        /**
         * Structure of output:
         * [0] -> column name
         * [1] -> HTML type name
         * [2] -> isIdentity
         * 
         * @param string $tableName
         * @return array
         */
        
        public function getTableColumns($tableName)
        {
            DB::connection()->getPdo();
            Cache::flush();
            $tableInfo = $this->getTableInfo($tableName);
            
            //database connection
            //TODO check authorization
            $columnNames = DbTables::getColumnNames($tableName);
            $columnInfo = array();
            for($i=0; $i< count($columnNames);$i++)
            {
                $columnInfo[$i] = array();
                $columnInfo[$i][0] = $columnNames[$i];
                $columnInfo[$i][1] = $this->mapTypeToHTML($this->getFieldType($tableName, $columnNames[$i]));
                $columnInfo[$i][2] = $tableInfo[$i][2];
                $columnInfo[$i][3] = DbTables::getDescriptionPossibleValues($tableName, $columnNames[$i]);
            }
            ////var_dump($columnInfo);
            return $columnInfo;
        } 
        
        public function countPrimaryKeys($tableContent) 
        {
            throw new \Exception("old metod, use countPrimaryKeys2 instead");
            if($tableContent[0][1][3] == true)return 2;
            else return 1;
        }   
        
        public static function countPrimaryKeys2($tableName){
            // TODO this function could be standalone -> basis on sql query
            $tableInfo = DatabaseController::getTableInfo($tableName);
            $counter = 0;
            foreach($tableInfo as $columnInfo){
                if ($columnInfo[4] === true) // identity flag
                    ++$counter;
            }
            return $counter;
        }
        
        
        public function countIndentityFields($tableName){
            $tableInfo = $this->getTableInfo($tableName);
            $counter = 0;
            foreach($tableInfo as $columnInfo){
                if ($columnInfo[2] === true) // identity flag
                    ++$counter;
            }
            return $counter;
        }


        /**
         * Structure of output:
         * [0] -> column name
         * [1] -> type (HTML)
         * [2] -> isIdentity
         * [3] -> possible [ID=>DESCRIPTION]
         * [4] -> isPrimaryKey
         * 
         * @param string $tableName
         * @return array
         */
        public static function getTableInfo($tableName){
            //database connection
            DB::connection()->getPdo();
            Cache::flush();
            
            //$viewName = DbTables::getViewName($tableName);
            $columns = DbTables::getColumnNames($tableName);
            
            $tableInfo = array();
            //$pkInRow = 0;

            foreach ($columns as $columnName) 
            {
                $pdo = DB::connection()->getPdo();
                $sql = "select columnproperty(object_id('".$tableName."'),'".$columnName."','IsIdentity')";
                $res = $pdo->query($sql)->fetch();
                
                $idIdentity = (bool)$res[0]; // in result is only 1 element

                $sql = "IF '$columnName' IN (SELECT COLUMN_NAME
                    FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                    WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA+'.'+CONSTRAINT_NAME), 'IsPrimaryKey') = 1
                    AND TABLE_NAME = '$tableName')
                        SELECT 1 ELSE SELECT 0";
                $res = $pdo->query($sql)->fetch();
                $isPrimaryKey = (bool)$res[0];
  
                $typeRaw = DatabaseController::getFieldType($tableName,$columnName);
                $type = DatabaseController::mapTypeToHTML($typeRaw);
                
                array_push($tableInfo, 
                        [
                            $columnName,
                            $type,
                            $idIdentity,
                            false, // TODO ???????
                            $isPrimaryKey
                        ]);
            }
            return $tableInfo;
        }

        /**
         * Structure of result
         * [i] -> row 
         * [i][j] -> column
         * [i][j][0] -> columnName
         * [i][j][1] -> value
         * [i][j][2] -> type(in HTML)
         * [i][j][3] -> isPrimaryKey
         * [i][j][4] -> isIdentity
         * [i][j][5] -> descriptionValue
         * [i][j][6] -> possible [ID=>DESCRIPTION]
         * 
         * @param string $tableName
         * @return array
         */
        public function getTableData($tableName)
        {
            DB::connection()->getPdo();
            Cache::flush();
            
            //database connection
            $viewName = DbTables::getViewName($tableName);
            $tableInfo = $this->getTableInfo($viewName);
            $columns = DbTables::getColumnNames($viewName);
            $results = DB::table($viewName)->get();    
            //$schema = \DB::getDoctrineSchemaManager();
            
            $tableContent = array();
            foreach ($results as $result) 
            {
                $innerarray = array();
                 //$columnsOfTypes = $schema->listTableColumns($tableName);
                $pkInRow = 0;
                $iter = 0;
                //foreach ($columns as $columnName) 
                for ($i=0; $i < count($columns); ++$i)
                {
                    $columnName = $columns[$i];
                    $columnValue = get_object_vars($result)[$columnName];
                    $idIdentity = $tableInfo[$i][2];
                    $type = $tableInfo[$i][1];
                    
                    // read description
                    $description = 
                            DbTables::getDecriptionValue($tableName, $columnName, $columnValue);
                    // TODO optimalization -> too many calls
                    $descriptionsPossibleValues = 
                            DbTables::getDescriptionPossibleValues($tableName, $columnName);
                    
                    $subarray = array();
                    array_push($subarray, 
                        $columnName, 
                        $columnValue,
                        $type, 
                        $this->isPrimaryKey($tableName, $columnName),
                        $idIdentity,
                        $description,
                        $descriptionsPossibleValues  
                    );
                      
                    array_push($innerarray, $subarray);
                    unset($subarray);
                    $iter++;
                }
                $this->primaryKeysInRow = $pkInRow;
                array_push($tableContent, $innerarray);
                unset($innerarray);
            }            
            //var_dump($tableContent);
            //echo $pkInRow;
            //echo $this->countPrimaryKeys($tableContent);
            return $tableContent;
        }        
        public function readPage() 
	{
            Cache::flush();
            //$cruds = $this->cruds;
            $roleName = RoleController::getSelectedRoleName();
            $tableName = TableController::getSelectedTableName();
            
            $tableContent=$this->getTableData($tableName);
            if (empty($tableContent))   
            {
                return redirect($this::FAILURE_REDIRECTION)
                    ->withErrors('Tabela jest pusta');
            }
            //$tableName = Translator::tablesToFrontend($tableName);
            return view('readPage', ['tableContent' => $tableContent, 
                                        'tableName' => $tableName,
                                         'roleName' => $roleName]);

	}
      
        //////////////////////////
        ////  Insert section  ////
        //////////////////////////
        public function toInsertArray($insertedValues,$tableColumns)
        { //prepare inserted values to database insert format 
            $insertArray = array();
            $i = 0;
            foreach ($insertedValues as $value) 
            {
                if ($value === '0' || !empty($value)){ // empty return false for $value === '0' 
                    $key = $tableColumns[$i][0];
                    $insertArray[$key] = $value;
                }
                $i++;
            }
            return $insertArray;
        }      
        public function displaySet ($insertedValues)
        {
            $stringText = ' ';
            foreach($insertedValues as $insertedValue)
            {

                $stringText= $stringText.$insertedValue.', ';
            }
            return $stringText;
        }
        public function insertData()
        { //when inserted data are being sent
            $roleName = RoleController::getSelectedRoleName();
            $tableName = TableController::getSelectedTableName();
            $tableView = DbTables::getViewName($tableName);
            $tableColumns = $this->getTableColumns($tableView);
            
            $insertedValues = array();     
            $password = '';
            
            if ($tableName === 'Uzytkownicy')
            {
                $login = Input::get($tableColumns[1][0]);
                $password = AuthController::generatePassword();
                $isLoggedCounter = Input::get($tableColumns[2][0]);
                $isLoggedCounter = 0;
                if (!AuthController::validateLogin($login))
                    return redirect($this::THIS_REDIRECTION)
                        ->withErrors('Niepoprawne dane. Użytkownik nie został dodany');

                $user = AuthController::create([
                        'login' => $login,
                        'password' => $password,
                ]);
                
                if (!empty($isLoggedCounter))
                    $user->setIsLoggedInCounter($isLoggedCounter);
                //$tableName = Translator::tablesToFrontend($tableName);
                return view('createPage', [
                    'tableColumns' => $tableColumns, 
                    'tableName' => $tableName,
                    'roleName' => $roleName,
                    'newLogin' => $login,
                    'newPassword' => $password
                    ])->with('success',['Utworzono użytkownika']);
                        
            }
            
            //validation
            foreach($tableColumns as $columnElement)
            {
                $element = Input::get($columnElement[0]);
                if ($columnElement[1] === 'checkbox'){
                    $element = $element === 'on' ? '1' : '0';
                }
             
                if ($element !== '')
                    array_push($insertedValues,$element);
                else
                    array_push($insertedValues,null);
            }
            DB::beginTransaction();
           try{
               //Database connection insert
                 DB::connection()->getPdo();
                 Cache::flush();
                 //cast inserted data to $key => $value
                 $insertArray = $this->toInsertArray($insertedValues, $tableColumns);
                 if (empty($insertArray))
                     throw new \Exception ("empty insert array");
                 //$tableName = Translator::tablesToSQL($tableName);
                 DB::table($tableName)->insert($insertArray);
                 DB::commit();
            } 
            catch (\Exception $ex) {

                DB::rollBack();
                return redirect($this::THIS_REDIRECTION)
                    ->withErrors('Niepoprawne dane. Wpis nie został dodany');
            }
            return redirect($this::THIS_REDIRECTION)
                    ->with('success', 'Wpis został pomyślnie dodany');
        }
        public function createPage()
        {
            Cache::flush();
            $roleName = RoleController::getSelectedRoleName();
            $tableName = TableController::getSelectedTableName();
            $viewName = DbTables::getViewName($tableName);
            $tableColumns=$this->getTableColumns($viewName);
            
            if(Input::has('addValuesCommand'))
            {  //when user has inserted data and pressed 'insert' 
                return $this->insertData();   
            }           
            //displaying the page
                        
               
            //$tableName = Translator::tablesToFrontend($tableName);
            return view('createPage', [
                'roleName' => $roleName,
                'tableName' => $tableName,
                'tableColumns' => $tableColumns,
                
                
            ]);      
        }
        
        //////////////////////////
        ////  Delete section  ////
        //////////////////////////
        public function deleteRow()
        {
            $columnName = Input::get('deleteColumnName');
            $columnName2 = Input::get('deleteColumnName2');
            $rowID = Input::get('deleteRowID');
            $rowID2 = Input::get('deleteRowID2');
            $tableName = TableController::getSelectedTableName();
            $keys = $this->countPrimaryKeys2($tableName);
            
            DB::beginTransaction();
            try
            {
                DB::connection()->getPdo();
                Cache::flush();
                $query = DB::table($tableName)->where($columnName, '=', $rowID);
                if ($keys === 2){
                    $query->where($columnName2, $rowID2);
                }
                if ($keys > 2)
                    throw new \Exception("not implement");
                
                $query->delete();
                        
                DB::commit();
                
            } catch (\Exception $ex) {
                DB::rollback();
                return redirect($this::THIS_REDIRECTION)
                    ->withErrors('Próba usunięcia powiązanego elementu. Usuwanie się nie powiodło.');
            }
                   
            return redirect($this::THIS_REDIRECTION)
                   ->with('success',['Rekord został usunięty']);      
        }
        public function deletePage() 
	{
            if (Input::has('deleteSelectedRow'))
            {
                return $this->deleteRow();               
            }
            
            Cache::flush();
            $roleName = RoleController::getSelectedRoleName();
            $tableName = TableController::getSelectedTableName();
            $tableContent=$this->getTableData($tableName);
            if (empty($tableContent))   
            {
                return redirect($this::FAILURE_REDIRECTION)
                        ->withErrors('Tabela jest pusta');
            }
            //$tableName = Translator::tablesToFrontend($tableName);
            return view('deletePage', ['tableContent' => $tableContent, 
                                        'tableName' => $tableName,
                                         'roleName' => $roleName]);
	}
        
        //////////////////////////
        ////  Modify section  ////
        //////////////////////////
        public function getModifiedRow($rowNumber)
        {
            $tableName = TableController::getSelectedTableName();
            $tableContent=$this->getTableData($tableName);
            $columnNumber =count($tableContent);
            $rowArray = array();
           
               for($i = 0; $i <$columnNumber; $i++)
               {
                   array_push($rowArray, Input::get($rowNumber.'#'.$i));
               }
            
            return $rowArray;
        }
        public function modifyTable(){
            $tableName = TableController::getSelectedTableName();
            $tableContent=$this->getTableData($tableName);
            $sqlLog = '';
            
            //$pkCount = $this->countPrimaryKeys($tableContent);
            $pkCount = $this->countPrimaryKeys2($tableName);
            
            DB::beginTransaction();
            try 
            {   
                for ($i = 0; $i < count($tableContent); $i++) 
                {
                    $row = $tableContent[$i];
                    $lastColumnIndex =count($tableContent[0])-1;
                    $updateMap = array();
                    
                    for($j = $lastColumnIndex; $j >= 0; $j--) 
                    {                  
                        $column = $row[$j];
                        $columnName = $column[0];
                        $value = $column[1];
                        $typeName = $column[2];
                        $isPrimaryKey = $column[3];
                        $isIdentity = $column[4];
                        $colName = $tableContent[$i][$j][0];
                        
                        
                        if($isIdentity == false){
                            if($typeName === 'checkbox') // possible answers - 'on' or null
                                $updateMap[$colName] = (is_null(Input::get($i.'#'.$j)) ? '0' : '1');
                            else 
                                $updateMap[$colName] = Input::get($i.'#'.$j);
                            
                        }
                    }                  
                    
                    $pk1 = $tableContent[$i][0][0]; //klucz główny nr 1
                    $valpk1 = $tableContent[$i][0][1];
                    
                    if ($pkCount == 1)
                    {             
                        DB::table($tableName)
                            ->where($pk1,$valpk1)
                            ->update($updateMap);
                    }
                    if ($pkCount == 2)
                    {
                        $pk2 = $tableContent[$i][1][0]; //klucz główny nr 2
                        $valpk2 = $tableContent[$i][1][1];    
                        DB::table($tableName)
                            ->where($pk1,$valpk1)->where($pk2,$valpk2)
                            ->update($updateMap);
                    }
                     unset($updateMap);
                }
                
                DB::commit();
                
            } catch (\Illuminate\Database\QueryException $ex) 
            {
                DB::rollBack();
                if($ex->getCode()==='23000')
                {
                    return redirect($this::THIS_REDIRECTION)
                    ->withErrors('Wystąpił błąd.');
            
                }
               return redirect($this::THIS_REDIRECTION)
                    ->withErrors('Wystąpił błąd. Wprowadzone dane są niepoprawne. ');
            }
            return redirect($this::THIS_REDIRECTION)
                    ->with('success',['Dokonano wprowadzone zmiany']);
                    //->withErrors('Dokonano wprowadzonych zmian'.$sqlLog);
        }
//        public function modifyTable(){
//            $tableName = TableController::getSelectedTableName();
//            $tableContent=$this->getTableData($tableName);
//            $sqlLog = '';
//            DB::beginTransaction();
//            try 
//            {   
//                for ($i = 0; $i < count($tableContent); $i++) 
//                {
//                    $row = $tableContent[$i];
//                    $lastColumnIndex =count($tableContent[0])-1;
//                    $sql = "UPDATE ".$tableName." SET ";
//                    for($j = $lastColumnIndex; $j >= 0; $j--) 
//                    {                  
//                        $column = $row[$j];
//                        $columnName = $column[0];
//                        $value = $column[1];
//                        $typeName = $column[2];
//                        $isPrimaryKey = $column[3];
//                        $isIdentity = $column[4];
//                        
//                        
//                        if($isIdentity == false){
//                            
//                            if($typeName === 'checkbox') // possible answers - 'on' or null
//                                $sql = $sql.$tableContent[$i][$j][0]."='".(is_null(Input::get($i.'#'.$j)) ? '0' : '1')."'";
//                            else 
//                                $sql = $sql.$tableContent[$i][$j][0]."='".(Input::get($i.'#'.$j))."'";
//                            
//                            $sql = $sql.', ';
//                        }
//                    }
//                    $sql = substr($sql, 0, -2); // remove last ','
//                    $pkCount = $this->countPrimaryKeys($tableContent);
//                    if ($pkCount == 1)
//                    {
//                        $sql = $sql." WHERE ".$tableContent[$i][0][0]."='".$tableContent[$i][0][1]."'";
//                        $sqlLog = $sqlLog.$sql; // TODO to remove
//                        $pdo = DB::connection()->getPdo();
//                        $result = $pdo->query($sql);
//                    }
//                    if ($pkCount == 2)
//                    {
//                        $sql = $sql." WHERE ".$tableContent[$i][0][0]."='".$tableContent[$i][0][1]."' AND ".$tableContent[$i][1][0]."='".$tableContent[$i][1][1]."'";
//                        $sqlLog = $sqlLog.$sql; // TODO to remove
//                        $pdo = DB::connection()->getPdo();
//                        $result = $pdo->query($sql);
//                    }
//                }
//                DB::commit();
//                
//            } catch (\Exception $e) 
//            {
//                DB::rollBack();
//                return redirect($this::THIS_REDIRECTION)
//                    ->withErrors('Wystąpił błąd. Wprowadzone dane są niepoprawne.');
//            }
//            return redirect($this::THIS_REDIRECTION)
//                    ->with('success',['Dokonano wprowadzone zmiany']);
//                    //->withErrors('Dokonano wprowadzonych zmian'.$sqlLog);
//        }

        public function modifyPage()
        {
            if (Input::has('modifyValuesCommand'))
            {
                return $this->modifyTable();
            }
            

            Cache::flush();
            $roleName = RoleController::getSelectedRoleName();
            $tableName = TableController::getSelectedTableName();
            $tableContent=$this->getTableData($tableName);
            if (empty($tableContent))   
            {
                return redirect($this::FAILURE_REDIRECTION)
                        ->withErrors('Tabela jest pusta');
            }
            //$tableName = Translator::tablesToFrontend($tableName);
            return view('modifyPage', ['tableContent' => $tableContent, 
                                        'tableName' => $tableName,
                                         'roleName' => $roleName]);
        }
        
        ////  CRUD controller ////        
        public function crudChoose()
        {
            Session::regenerate();
            
            if (Session::has(SessionFields::CRUD_NAME))
            {
                DbTables::refreshDb(); // refresh db before operation
                $crudName = Session::get(SessionFields::CRUD_NAME);
                switch($crudName)
                {
                    case Permissions::READ:
                        return $this->readPage();
                    case Permissions::CREATE:
                        return $this->createPage();
                    case Permissions::DELETE:
                        return $this->deletePage();
                    case Permissions::UPDATE:
                        return $this->modifyPage();
                }                
            }
            else
            {
                return redirect($this::FAILURE_REDIRECTION)
                        ->withErrors("Błąd wyboru operacji!".$crudName);
            }                
            
        }     
            

}