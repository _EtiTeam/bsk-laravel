<?php namespace App\Http\Controllers;

use Auth;
use Input;
use Session;

use App\Role;
use App\SessionFields;
use App\Http\Controllers\Controller;

class RoleController extends Controller {


    const FAILURE_REDIRECTION = '/roles';
    const SUCCESS_REDIRECTION = '/tables';

    
    public function __construct() {
        $this->middleware('App\Http\Middleware\RedirectIfRoleSelected');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function eliminateForbiddenRoles($table)
    {
         // check if user has already logged as some role        
        $currentUserRole = Auth::user()->role;
        if (!is_null($currentUserRole))
        {
            for ($i=0; $i< count($table)+1; $i++){
                if($table[$i]['NazwaRoli'] !== $currentUserRole['NazwaRoli'])
                {                  
                    unset($table[$i]);                
                }
            }            
        }
        return $table;
    }
    public function index()
    {
        $table = Auth::user()->roles->toArray();        
        $table = $this-> eliminateForbiddenRoles($table);        
        return view('rolesPage', ['table' => $table]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Input::has('roleName'))
            return redirect()->guest($this::FAILURE_REDIRECTION);
        $roleName = Input::get('roleName');

        // check if user has this role
        $role = Auth::user()->roles->where('NazwaRoli', $roleName)->first();
        if (is_null($role))
            return redirect()->guest($this::FAILURE_REDIRECTION);

        // check if user has already logged as some role        
        $currentUserRole = Auth::user()->role;
        if (is_null($currentUserRole))
        {
            $this::loginAsRole($role);
        }
        else // if user has already logged in
        {
            if ($role->getKey() === $currentUserRole->getKey())
                $this::loginAsRole($role);
            else
            {
                // TODO - error information
                return redirect()
                    ->guest($this::FAILURE_REDIRECTION)
                    ->withErrors("Jesteś już zalogowany na inną rolę"); 
            }
        }

        return redirect()->guest($this::SUCCESS_REDIRECTION);
    }

    /**
     * 
     * @return string|null
     */
    
    public static function getSelectedRoleName(){
        $rola = RoleController::getSelectedRole();
        return is_null($rola) ? null : $rola->NazwaRoli;
    }
    
    public static function getSelectedRoleId(){
        Session::regenerate();
        return Session::get(SessionFields::ROLE_ID);
    }

    public static function getSelectedRole(){
        return Role::find(RoleController::getSelectedRoleId());
    }
    
    public static function loginAsRole(Role $role){
        Session::regenerate();
        Auth::user()->setRole($role);        
        Auth::user()->setIsLoggedAs(true);
        Session::put(SessionFields::ROLE_ID, (string)$role->getKey());
    }
    
    public static function isRoleSelected(){
        Session::regenerate();
        $a = Session::get(SessionFields::ROLE_ID);
        return !is_null($a);
    }

}
