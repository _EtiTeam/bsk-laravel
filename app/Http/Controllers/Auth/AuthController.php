<?php namespace App\Http\Controllers\Auth;

use DB;
use Hash;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Auth;
use App\User;
use App\SessionFields;
use Illuminate\Http\Request;
use App\Http\Controllers\RoleController;
use App\Http\Middleware\Authenticate as AuthenticateMiddleware;

class AuthController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    //private $LOGIN_VIEW = 'auth/login';
    //private $REGISTER_VIEW = 'auth/register';

    //private $SUCCESS_CONTROLLER = 'dashboard';    
    
    const MIN_PASSWORD_LENGTH = 8;
    const MIN_LOGIN_LENGTH = 2;
    const MAX_LOGIN_LENGTH = 50;
    
    const LOGIN_VIEW = 'loginPage';
    const REGISTER_VIEW = 'auth/register';

    const SUCCESS_REDIRECTION = '/roles'; // warning: guest middleware can also redirect
    const FAILURE_REDIRECTION = '/';
    const REGISTER_REDIRECTION = '/register';



    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
            $this->auth = $auth;
            $this->registrar = $registrar;

            $this->middleware('guest', ['except' => 'getLogout']);
    }
    
    /**
     * Force login and set flags in databases
     * 
     * @param \App\Http\Controllers\Auth\Authenticatable $user
     * @return boolean
     */
    private function login(User $user){
        // check if user is already logged
        if (Auth::check()){
            $success = true;
        }
        else{
            $this::logout();
            // manualy logging
            $userId = $user->getAuthIdentifier();
            $success = $this->auth->loginUsingId($userId);
        }
        
        if (!is_null($success))
        {
            $user->refresh();
            
            $user->setIsLoggedIn(true);
            $user->updateActivityTime();
            return true;
        }
        return false;
    }
    
    /**
     * Logout and set flags in databases
     * 
     * @param \App\Http\Controllers\Auth\Authenticatable $user
     * @return boolean
     */
    
    public static function logoutAllTerminals(){
        if (Auth::check())
        {
            Auth::user()->logoutAllTerminals();
            AuthController::logout();
            return true;
        }
        return false;
    }
    
    
    public static function logout(){
        if (Auth::check())
        {
            DB::transaction(function (){
                Auth::user()->setIsLoggedIn(false);
                if (RoleController::isRoleSelected())
                    Auth::user()->setIsLoggedAs(false);
            });
            
            SessionFields::clearAuthorizationData(); // remove all data of authorization
            Auth::logout();
            return true;
        }
        SessionFields::clearAuthorizationData(); // remove all data of authorization
        return false;
    }
        
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  Request  $request
     * @return bool
     */
    public static function validateRegisterRequest(Request $request)
    {
        $minPass = AuthController::MIN_PASSWORD_LENGTH;
        $minLogin = AuthController::MIN_LOGIN_LENGTH;        
        $maxLogin = AuthController::MAX_LOGIN_LENGTH;

        $validator = Validator::make($request->all(), [
            'login' => "required|min:$minLogin|max:$maxLogin|unique:Uzytkownicy",
            'password' => "required|confirmed|min:$minPass",
        ], array()); // last argument is message 
        
        return !$validator->fails();
    }
    
    public static function validatePasswordRequest(Request $request){
        $minPass = AuthController::MIN_PASSWORD_LENGTH;
        
        $validator = Validator::make($request->all(), [
            'password' => "required|min:$minPass|confirmed",
        ], array()); // last argument is error message 
        
        return !$validator->fails();
    }
    
    public static function validatePassword($password){
        $minPass = AuthController::MIN_PASSWORD_LENGTH;
        
        $validator = Validator::make(['password' => $password], [
            'password' => "required|min:$minPass",
        ], array()); // last argument is error message 
        
        return !$validator->fails();
    }
    
    public static function validateLogin($login){
        $minLogin = AuthController::MIN_LOGIN_LENGTH;        
        $maxLogin = AuthController::MAX_LOGIN_LENGTH;
        
        $array = ['login' => $login];
        $validator = Validator::make($array, [
            'login' => "required|min:$minLogin|max:$maxLogin|unique:Uzytkownicy",
        ], array()); // last argument is error message 
        
        return !$validator->fails();
    }

    /**
     * Setting password with validation and hashing
     * 
     * @param string
     * @throws \InvalidArgumentException|null
     */
    public static function setPassword($newPassword){
        if (!AuthController::validatePassword($newPassword))
            throw new \InvalidArgumentException('$newPassword is invalid');
        
        $hash = Hash::make($newPassword);
        Auth::user()->setPassword($hash);
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public static function generatePassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $newPassword = implode($pass); //to string
        
        return $newPassword;
    }
    
    public static function create(array $data)
    {        
        return User::create([
            'login' => $data['login'],
            'password' => Hash::make($data['password']),
        ]);
    }
     
    public static function refresh(User $user){
        // check if no-one is logged
        if ($user->getIsAnyoneLoggedIn() === false || 
            AuthenticateMiddleware::isTooLongInactive($user))
        {
            $user->logoutAllTerminals();
        }
    }
    
    /* Login get post methods */
    protected function getLogin() {
        return View($this::LOGIN_VIEW);
    }

    protected function postLogin(Request $request) {        
        if ($this->auth->attempt($request->only('login', 'password'))) 
        {
            $this->login($this->auth->user()); // only for set flags in database
            //return redirect()->route($this->SUCCESS_CONTROLLER);
            return redirect()->guest($this::SUCCESS_REDIRECTION);
        }

        return redirect($this::FAILURE_REDIRECTION)->withErrors("Błąd");
    }

    /* Register get post methods */
    protected function getRegister() {
        return View($this::REGISTER_VIEW);
    }

    protected function postRegister(Request $request)
    {
        if ($this->validateRegisterRequest($request))
        {
            $success = false;
            DB::transaction(function() use ($request, $success){
                $user = AuthController::create($request->all());
                $success = $this->login($user);
                Auth::user()->setIsRequiredPasswordChange(false);
            });
                    
            if ($success) 
                return redirect($this::SUCCESS_REDIRECTION);
            else 
                return redirect($this::REGISTER_REDIRECTION)->withErrors("Błąd");
                
        }
        return redirect($this::REGISTER_REDIRECTION)->withErrors("Blad");
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    protected function getLogout()
    {
        $this->logout();
        return redirect($this::FAILURE_REDIRECTION);
    }

}
