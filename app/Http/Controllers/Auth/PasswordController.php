<?php namespace App\Http\Controllers\Auth;

use DB;
use Hash;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller {

    const SUCCESS_REDIRECTION = '/roles'; // warning: guest middleware can also redirect
    const FAILURE_REDIRECTION = '/reset';
    //use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
     * @return void
     */
//	public function __construct(Guard $auth, PasswordBroker $passwords)
//    {
//            $this->auth = $auth;
//            $this->passwords = $passwords;
//
//            $this->middleware('guest');
//	}
    public function getResetPassword()
    {
        return view('resetPassword');
    }

    public function postResetPassword(Request $request)
    {
        $newPassword = $request->get('password');
        $oldPassword = $request->get('oldPassword');
        $hash = Auth::user()->getAuthPassword();
        
        $isNewPasswordValid = AuthController::validatePasswordRequest($request);
        $isOldPasswordValid = Hash::check($oldPassword, $hash);
        
        if($isOldPasswordValid && $isNewPasswordValid){
            DB::transaction(function() use ($newPassword){
                AuthController::setPassword($newPassword);
                Auth::user()->setIsRequiredPasswordChange(false);
            });
            
            return redirect()->guest($this::SUCCESS_REDIRECTION)
                ->with('success', "Hasło zostało zmienione");
        }
        
        return redirect()->guest($this::FAILURE_REDIRECTION)
                ->withErrors("Błąd");
    }
        

}
