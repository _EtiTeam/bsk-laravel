<?php namespace App\Http\Controllers;


use Auth;
use Input;
use Session;

use App\DbTables;
use App\SessionFields;
use App\Http\Controllers\Controller;
use App\Translator;

class TableController extends Controller {

    const FAILURE_REDIRECTION = '/tables';
    const SUCCESS_REDIRECTION = '/tables/crud';
    
    public function __construct() {
        //$this->middleware('App\Http\Middleware\RedirectIfTableSelected');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //try 
        //{
            $roleName = RoleController::getSelectedRoleName();
            $tables = Auth::user()->getUserTables();
           // $tables = Translator::tablesToFrontend($tables);
            return view('tablesPage', ['tables' => $tables, 'roleName' => $roleName]);
        //} 
        //catch (QueryException $e) 
        //{
        //    NotificationsController::errorBox('Błąd połączenia z bazą danych');
        //    return view('tablesPage')->withErrors('Błąd połączenia z bazą danych');
        //}

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Input::has('tableName'))
            return redirect()->guest($this::FAILURE_REDIRECTION);

        $tableName = Input::get('tableName'); //get value from input
       // $tableName = Translator::tablesToSQL($tableName);
        if (!DbTables::contains($tableName))
            return redirect()->guest($this::FAILURE_REDIRECTION);

        if (!Auth::user()->hasAccessTo($tableName))
            return redirect()->guest($this::FAILURE_REDIRECTION);

        //saving to session
        Session::regenerate();
        Session::put(SessionFields::TABLE_NAME, $tableName);
        return redirect()->guest($this::SUCCESS_REDIRECTION);
    }
    
    /**
     * 
     * @return string|null
     */
    public static function getSelectedTableName(){
        Session::regenerate();
        return Session::get(SessionFields::TABLE_NAME);
    }

    /**
     * 
     * @return boolean
     */
    public static function isTableSelected(){
        Session::regenerate();
        $s = Session::get(SessionFields::TABLE_NAME);
        return !is_null($s);
    }

}
