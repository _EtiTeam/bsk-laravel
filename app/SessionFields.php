<?php

namespace App;

use Session;

use App\User;
use App\Permissions;

abstract class SessionFields {
    const ROLE_ID = 'auth.role.id';
    const TABLE_NAME = 'auth.table.name';
    const CRUD_NAME = 'auth.crud.name';
    const AUTH_DATA = 'auth';
    
    public static function clearAuthorizationData(){
        Session::regenerate();
//        $session1 = Session::get('auth');
        Session::forget(SessionFields::AUTH_DATA); // remove all data of authorization
//        
//        Session::put(SessionFields::ROLE_ID, 1);
//        $session2 = Session::get('auth');
//        $i =4;
//        echo $i;
    }
}
