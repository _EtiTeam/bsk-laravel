<?php

namespace App\Providers;

use App\Sha512Hasher;
use Illuminate\Hashing\HashServiceProvider;


/**
 * Description of Sha2HashServiceProvider
 *
 * @author Daniel
 */

class Sha512HashServiceProvider extends HashServiceProvider {
    public function register()
    {
        $this->app->singleton('hash', function() { return new Sha512Hasher; });
    }
}
