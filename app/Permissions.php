<?php

namespace App;

use \App\User;

/**
 * Description of Permissions
 *
 * @author Daniel
 */
class Permissions {
    /**
     *
     * @var string 
     */
    private $tableName;
    
    /**
     *
     * @var \App\User 
     */
    private $user;
    
    public function __construct($tableName, User $user){
        $this->tableName = $tableName;
        $this->user = $user;
    }
    
    /**
     * 
     * @return boolean
     */
    public function anyPermission(){
        return 
            $this->isReadPermission() ||
            $this->isCreatePermission() ||
            $this->isUpdatePermission() ||
            $this->isDeletePermission();
    }
    
    /**
     * 
     * @return boolean
     */
    public function isReadPermission(){
        return $this->isPermission(Permissions::READ);
    }
    
    /**
     * 
     * @return boolean
     */
    public function isCreatePermission(){
        return $this->isPermission(Permissions::CREATE);        
    }
    
    /**
     * 
     * @return boolean
     */
    public function isUpdatePermission(){        
        return $this->isPermission(Permissions::UPDATE);
    }
    
    /**
     * 
     * @return boolean
     */
    public function isDeletePermission(){        
        return $this->isPermission(Permissions::DELETE);
    }
    
    public function getPermissionNames(){
        $array = array();
        if ($this->isCreatePermission()) array_push($array, $this::CREATE);
        if ($this->isReadPermission()) array_push($array, $this::READ);
        if ($this->isUpdatePermission()) array_push($array, $this::UPDATE);
        if ($this->isDeletePermission()) array_push($array, $this::DELETE);
        return $array;
    }
    
    /**
     * check permission user, which is logged as specyfic role
     * one permission could based on more than one permission flag in database
     * 
     * @param string $crudOperation (based on constants in Permission class)
     * @return boolean|\LogicException
     */
    public function isPermission($crudOperation){
        if(is_null($this->user->role))
            throw new \LogicException('User hasn\'t got any role');
        
        // this permission must exist
        $permissionsToCheck = $this::$permissionDependency[$crudOperation];
        if(is_null($permissionsToCheck))
            throw new \InvalidArgumentException('Bad $crudOperation argument');
        
        // must be at least one permission
        if(empty($permissionsToCheck))
            return false;
        
        foreach($permissionsToCheck as $permissionName){
            $columnName = $permissionName.$this->tableName; // column name in Role table   
            $result = $this->user->role->getAttribute($columnName);
            if ($result === '0')
                    return false;
        }
   
        return true; // if all permissions in dependency array are true
    }
    
    public static function isCrudOperation($str){
        return 
            Permissions::CREATE === $str ||
            Permissions::READ === $str ||
            Permissions::UPDATE === $str ||
            Permissions::DELETE === $str;
    }
    
    const READ = 'Read';    
    const CREATE = 'Create';
    const UPDATE = 'Update';
    const DELETE = 'Delete';

    private static $permissionDependency  = [
        // in application => [in database],
        Permissions::READ => [Permissions::READ],
        Permissions::CREATE => [Permissions::CREATE],
        Permissions::UPDATE => [Permissions::UPDATE, Permissions::READ],
        Permissions::DELETE => [Permissions::DELETE, Permissions::READ],
    ];
}
