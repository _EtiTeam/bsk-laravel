<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

use DB;
use App\Permissions;
use App\Http\Controllers\Auth\AuthController;

class User extends Model implements AuthenticatableContract {

    use Authenticatable;

    /**
     * 
     * @return string
     */
    static function getLastActivityColumnName(){
        return 'DateTimeOstAktywnosci';
    }
    
    /**
     * 
     * @return string
     */
    static function getIsLoggedAsFlagColumnName(){
        return 'CzyWybranaRola';
    }
    
    /**
     * 
     * @return string
     */
    static function getIsLoggedInFlagColumnName(){
        return 'CzyZalogowany';
    }

    /**
     * 
     * @return string
     */
    static function getLogAsColumnName(){
        return 'ZalogowanyNaRole';
    }
    
    /**
     * 
     * @return string
     */
    static function getPasswordColumnName(){
        return 'password';
    }
    
    /**
     * 
     * @return string
     */
    static function getIsRequiredPasswordChangeColumnName(){
        return 'WymaganaZmianaHasla';
    }

    /**
     * Logout all possibly terminals of this account (by database) 
     */
    public function logoutAllTerminals(){
        DB::transaction(function(){
            $this->setIsLoggedInCounter(0);
            $this->setIsLoggedAsCounter(0);
            $this->setRole(null);
            $this->save();
        });
    }
    
    /**
     * It reads logged flag from database 
     * 
     * @return boolean
     */
    public function getIsAnyoneLoggedIn(){
        return $this->getAttribute(User::getIsLoggedInFlagColumnName()) > 0;
    }

    /**
     * 
     * @param boolean $ifSet
     */
    public function setIsLoggedIn($ifSet){
        DB::transaction(function() use ($ifSet) {
            $counter = $this->getAttribute(User::getIsLoggedInFlagColumnName());

            // change counter
            if ($ifSet === true) ++$counter;
            else if ($ifSet === false) --$counter;
            else throw new \InvalidArgumentException("boolean argument expected");
            
            // minimum value
            if ($counter < 0) $counter = 0;

            if ($counter <= 0){
                // reset role data
                $this->setRole(null);
                $this->setIsLoggedAsCounter(0);
            }
            
            // set new counter
            $this->setIsLoggedInCounter($counter);
        });
    }
    
    public function setIsLoggedInCounter($newValue){
        $this->setAttribute(User::getIsLoggedInFlagColumnName(), $newValue);
        $this->save();
    }
    
        /**
     * It reads logged flag from database 
     * 
     * @return boolean
     */
    public function getIsAnyoneLoggedAs(){
        $condition1 = !is_null($this->role);
        $condition2 = $this->getAttribute(User::getIsLoggedAsFlagColumnName()) > 0;
        if ($condition1 !== $condition2) 
            throw new \LogicException("something went wrong");
        
        return $condition1;
    }

    /**
     * 
     * @param boolean $ifSet
     */
    public function setIsLoggedAs($ifSet){
        DB::transaction(function() use ($ifSet) {
            $counter = $this->getAttribute(User::getIsLoggedAsFlagColumnName());

            // change counter
            if ($ifSet === true) ++$counter;
            else if ($ifSet === false) --$counter;
            else throw new \InvalidArgumentException("boolean argument expected");
            
            // minimum value
            if ($counter < 0) $counter = 0;

            if ($counter <= 0)
                $this->setRole(null);
            
            // set new counter
            $this->setIsLoggedAsCounter($counter);
        });
    }
    
    public function setIsLoggedAsCounter($newValue){        
        $this->setAttribute(User::getIsLoggedAsFlagColumnName(), $newValue);
        $this->save();
    }

    /**
     * 
     * @param \App\Role $newRole
     * @return null|int
     */
    public function getRole(Role $newRole){
        return $this->getAttribute(User::getLogAsColumnName());
    }
    
    /**
     * 
     * @param null|\App\Role $newRole
     * @throws \InvalidArgumentException
     */
    public function setRole($newRole){
        if (!is_null($newRole) && !$newRole instanceOf Role)
            throw new \InvalidArgumentException('$newRole must be null or Role instance');
        
        if ($newRole instanceOf Role)
            $newRole = $newRole->getKey();
            
        $this->setAttribute(User::getLogAsColumnName(), $newRole);
        $this->save();
    }
    
    /**
     * It reads last activity time from database 
     * 
     * @return \DateTime
     */
    public function getLastActivityTime(){
        $lastActivityDateTimeString = $this->getAttribute(User::getLastActivityColumnName());
        return new \DateTime($lastActivityDateTimeString);
    }

    /**
     * Set curretly time as a last activity time
     */
    public function updateActivityTime(){
        $this->setAttribute(
                User::getLastActivityColumnName(), 
                date("Y-m-d H:i:s")
        );
        $this->save();
    }
    
    /**
     * Setting raw password
     * 
     * @param string
     */
    public function setPassword($newPassword){
        $this->setAttribute(User::getPasswordColumnName(), $newPassword);
        $this->save();
    }
    
    /**
     * 
     * @return boolean
     */
    public function getIsRequiredPasswordChange(){
        return $this->getAttribute(User::getIsRequiredPasswordChangeColumnName()) > 0;
    }

    /**
     * 
     * @param boolean $newValue
     * @throws \InvalidArgumentException
     */
    public function setIsRequiredPasswordChange($newValue){
        if (!is_bool($newValue))
            throw new \InvalidArgumentException("boolean argument expected");
        $this->setAttribute(User::getIsRequiredPasswordChangeColumnName(), $newValue);
        $this->save();
    }
    
    /**
     * 
     * @return array
     */
    public function getUserTables(){
        return DbTables::getTablesForUser($this);
    }
    
    /**
     * 
     * @param string $tableName
     * @return boolean
     */
    public function hasAccessTo($tableName){
        if(!DbTables::contains($tableName))
            return false;
        $permissions = $this->getPermissions($tableName);
        return $permissions->anyPermission();
    }
    
    public function role(){
        return $this->belongsTo('App\Role', 'ZalogowanyNaRole', 'IDRola');
    }
    
    public function roles(){
        return $this->belongsToMany('App\Role', 'RolaAUzytkownik', 'IDUzytkownik', 'IDRola');
    }

    // Disabling token remember functionality
    public function getRememberToken() {
         return null; // not supported
    }

    public function getRememberTokenName() {
        return null; // not supported
    }

    public function setRememberToken($value) {
        // not supported
    }
    
    public function getPermissions($tableName){
        return new Permissions($tableName, $this);
    }
    
    public function refresh(){
        AuthController::refresh($this);
    }

    const ID_COLUMN = 'id';
    const TABLE_NAME = 'Uzytkownicy';
    const TABLE_VIEW_NAME = 'UzytkownicyView';
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = User::TABLE_NAME;
    
    /**
     * The database table primary key column name
     * 
     * @var string 
     */
    protected $primaryKey = User::ID_COLUMN;

    public $timestamps = false;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['login', 'password'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['password', 'ZalogowanyNaRole'];
    
    protected $guarded = ['id'];
}
